import React from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
} from "react-router-dom";
import {
  makeObservable,
  observable,
  observer,
  computed,
  action,
  flow,
} from "mobx-react";

const Editing = observer((props) => {
  const { vehicleID } = useParams();
  return (
    <div
      style={{
        maxWidth: "500px",
        width: "calc(100% - 8px)",
        marginLeft: "auto",
        marginRight: "auto",
      }}
    >
      <h1>Editing Details of Vehicle #{vehicleID}</h1>
      <table style={{ width: "100%" }}>
        <tr>
          <td>ID:</td>
          <td>
            <input
              style={{ width: "calc(100% - 15px)" }}
              value={props.currentID}
              onChange={(event) => {
                props.setCurrentID(event.target.value);
              }}
            ></input>
          </td>
        </tr>
        <tr>
          <td>Name:</td>
          <td>
            <input
              style={{ width: "calc(100% - 15px)" }}
              value={props.currentName}
              onChange={(event) => {
                props.setCurrentName(event.target.value);
              }}
            ></input>
          </td>
        </tr>
        <tr>
          <td>Abbreviation:</td>
          <td>
            <input
              style={{ width: "calc(100% - 15px)" }}
              value={props.currentAbbreviation}
              onChange={(event) => {
                props.setCurrentAbbreviation(event.target.value);
              }}
            ></input>
          </td>
        </tr>
        <tr>
          <td>Image URL:</td>
          <td>
            <input
              style={{ width: "calc(100% - 15px)" }}
              value={props.currentImageURL}
              onChange={(event) => {
                props.setCurrentImageURL(event.target.value);
              }}
            ></input>
          </td>
        </tr>
      </table>
      <Link to="/">
        <button
          style={{
            width: "calc(50% - 4px)",
            float: "left",
            backgroundColor: "#ccffcc",
            fontWeight: "bold",
            height: "3em",
          }}
          onClick={props.onSubmit}
        >
          Submit
        </button>
        <button
          style={{
            width: "calc(50% - 4px)",
            fontWeight: "bold",
            backgroundColor: "#ffcccc",
            float: "right",
            height: "3em",
          }}
        >
          Cancel
        </button>
      </Link>
    </div>
  );
});

export { Editing };
