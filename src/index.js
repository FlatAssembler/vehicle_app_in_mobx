import React from "react";
import ReactDOM from "react-dom";
import { observable } from "mobx";
import { VehicleList } from "./VehicleList.js";
import { VehicleModel } from "./VehicleModel.js";
import { App } from "./App.js";
import "./index.css";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
