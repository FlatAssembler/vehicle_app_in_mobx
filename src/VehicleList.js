import React from "react";
import ReactDOM from "react-dom";
import { VehicleModel } from "./VehicleModel.js";
import { VehicleListElement } from "./VehicleListElement.js";
import {
  makeObservable,
  observable,
  observer,
  computed,
  action,
  flow,
} from "mobx-react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
} from "react-router-dom";

const VehicleList = observer((props) => {
  const vehicleList = props.vehicleList;
  if (!Array.isArray(vehicleList))
    throw new TypeError(
      '"vehicleList" is not an array, it is a "' + typeof vehicleList + '"!'
    );
  return (
    <div className="VehicleModelList">
      {vehicleList.map((vehicleModel) => {
        if (vehicleModel instanceof VehicleModel)
          return (
            <Link to={`/edit${vehicleModel.id}`}>
              <VehicleListElement
                onClick={() => {
                  props.setCurrentID(vehicleModel.id);
                  props.setCurrentName(vehicleModel.name);
                  props.setCurrentAbbreviation(vehicleModel.abbreviation);
                  props.setCurrentImageURL(vehicleModel.imageURL);
                }}
                vehicleModel={vehicleModel}
                key={
                  vehicleModel.id /* https://robinpokorny.medium.com/index-as-a-key-is-an-anti-pattern-e0349aece318 */
                }
              />
            </Link>
          );
        else
          throw TypeError(
            'One of the elements of "vehicleList" is not "VehicleModel"!'
          );
      })}
    </div>
  );
});

export { VehicleList };
