import React from "react";
import ReactDOM from "react-dom";
import { VehicleModel } from "./VehicleModel.js";
import {
  makeObservable,
  observable,
  observer,
  computed,
  action,
  flow,
} from "mobx-react";

const VehicleListElement = observer((props) => {
  const vehicleModel = props.vehicleModel;
  if (!(vehicleModel instanceof VehicleModel))
    throw new TypeError("vehicleModel is not of the type VehicleModel!");
  return (
    <div className="VehicleListElement" onClick={props.onClick}>
      <div className="BigVehicleID">
        {vehicleModel.imageURL ? (
          <img src={vehicleModel.imageURL} alt={vehicleModel.name} />
        ) : (
          "#" + vehicleModel.id
        )}
      </div>
      <div className="VehicleID">ID: #{vehicleModel.id}</div>
      <div className="VehicleName">
        <span style={{ fontSize: "0.5em" }}>Name:</span>
        <br />
        <b>{vehicleModel.name}</b>
      </div>
      <div className="VehicleAbbreviation">
        abbreviation:
        <br />
        {vehicleModel.abbreviation}
      </div>
    </div>
  );
});

export { VehicleListElement };
