import { makeObservable, observable, computed, action, flow } from "mobx";

class VehicleModel {
  constructor(id, name, abbreviation, imageURL) {
    this.id = id;
    this.name = name;
    this.abbreviation = abbreviation;
    this.imageURL = imageURL;
    makeObservable(this, {
      id: observable,
      name: observable,
      abbreviation: observable,
      imageURL: observable,
    });
  }
  toString = () => `#${this.id} "${this.name}" "${this.abbreviation}"`;
}

export { VehicleModel };
