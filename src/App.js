import React from "react";
import ReactDOM from "react-dom";
import { observable } from "mobx";
import { VehicleList } from "./VehicleList.js";
import { VehicleModel } from "./VehicleModel.js";
import { Editing } from "./Editing.js";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
} from "react-router-dom";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      originalArrayOfVehicles: observable([
        new VehicleModel(
          1,
          "Chevy Silverado 1500",
          "Subaru",
          "https://d.newsweek.com/en/full/1766114/subaru-outback.webp?w=790&f=8c3befa884eb966ee52d7f648864ad74"
        ),
        new VehicleModel(
          2,
          "Ford Escape",
          "Crossy",
          "https://d.newsweek.com/en/full/1766123/ford-escape.webp?w=790&f=a7d8249e5afd8042d2060e30a5cbf88a"
        ),
        new VehicleModel(
          3,
          "Honda Accord",
          "Acka",
          "https://d.newsweek.com/en/full/1766129/honda-accord.webp?w=790&f=d08fb48ef70a798545c749c1ab0c5b65"
        ),
        new VehicleModel(
          4,
          "Jeep Wrangler",
          "Jeep",
          "https://d.newsweek.com/en/full/1766134/jeep-wrangler.webp?w=790&f=db467be44fcfaaeb223d1d6e3e60219e"
        ),
        new VehicleModel(
          5,
          "Jeep Grand Cherokee",
          "Cherok",
          "https://d.newsweek.com/en/full/1766137/jeep-grand-cherokee.webp?w=790&f=bd8bb444114da9ef092522f291301ee8"
        ),
        new VehicleModel(
          6,
          "Toyota Highlander",
          "Highy",
          "https://d.newsweek.com/en/full/1766139/toyota-highlander-most-popular-car-us-models.webp?w=790&f=e912396d6e2fa9b14dcf215d6dd12213"
        ),
        new VehicleModel(
          7,
          "Ford Explorer",
          "Expy",
          "https://d.newsweek.com/en/full/1766150/nissan-rogue.webp?w=790&f=6ccd95259fe6b9433859f6fd5534d974"
        ),
        new VehicleModel(
          8,
          "Nissan Rogoue",
          "Rogee",
          "https://d.newsweek.com/en/full/1766151/toyota-corolla.webp?w=790&f=bb119a4498df63492c4038b6cf87cedf"
        ),
      ]),
      stringForFiltering: "",
      sortingCriterion: "ID",
      currentID: "",
      currentName: "",
      currentAbbreviation: "",
      currentImageURL: "",
    };
  }
  setCurrentID = (str) => {
    this.setState({
      currentID: str,
    });
  };
  setCurrentName = (str) => {
    this.setState({
      currentName: str,
    });
  };
  setCurrentAbbreviation = (str) => {
    this.setState({
      currentAbbreviation: str,
    });
  };
  setCurrentImageURL = (str) => {
    this.setState({
      currentImageURL: str,
    });
  };
  handleSubmit = () => {
    const newVehicleModel = new VehicleModel(
      this.state.currentID,
      this.state.currentName,
      this.state.currentAbbreviation,
      this.state.currentImageURL
    );
    let newArrayOfVehicles = this.state.originalArrayOfVehicles.slice();
    let ID_exists = false;
    for (let i = 0; i < newArrayOfVehicles.length; i++) {
      if (newArrayOfVehicles[i].id == newVehicleModel.id) {
        newArrayOfVehicles[i] = newVehicleModel;
        ID_exists = true;
      }
    }
    if (!ID_exists) newArrayOfVehicles.push(newVehicleModel);
    this.setState({ originalArrayOfVehicles: newArrayOfVehicles });
  };
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/edit:vehicleID">
            <Editing
              currentID={this.state.currentID}
              currentName={this.state.currentName}
              currentAbbreviation={this.state.currentAbbreviation}
              currentImageURL={this.state.currentImageURL}
              setCurrentID={this.setCurrentID}
              setCurrentName={this.setCurrentName}
              setCurrentImageURL={this.setCurrentImageURL}
              setCurrentAbbreviation={this.setCurrentAbbreviation}
              onSubmit={this.handleSubmit}
            />
          </Route>
          <Route path="/">
            <table
              style={{
                height: window.innerHeight - 16 + "px",
                marginLeft: "auto",
                marginRight: "auto",
              }}
            >
              <tr id="rowWithHeaderAndForm">
                <td>
                  <h1>Popular Vehicles</h1>
                  <form>
                    <table>
                      <tr>
                        <td>
                          <label htmlFor="filtering">Filter: </label>
                        </td>
                        <td>
                          <input
                            id="filtering"
                            onChange={(event) => {
                              this.setState({
                                stringForFiltering: event.target.value,
                              });
                            }}
                            value={this.state.stringForFiltering} //Needed in case of routing, or else it will clear when the user presses "Cancel" or "Submit".
                          />
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label htmlFor="sorting">Sort by:</label>
                        </td>
                        <td>
                          <select
                            id="sorting"
                            onChange={(event) => {
                              this.setState({
                                sortingCriterion: event.target.value,
                              });
                            }}
                            value={this.state.sortingCriterion} //Needed in case of routing, or else it will clear when the user presses "Cancel" or "Submit".
                          >
                            <option value="ID">ID</option>
                            <option value="name">name</option>
                            <option value="abbreviation">abbreviation</option>
                          </select>
                        </td>
                      </tr>
                    </table>
                  </form>
                </td>
              </tr>
              <tr>
                <td
                  style={{
                    height:
                      Math.max(
                        window.innerHeight -
                          (document.getElementById("rowWithHeaderAndForm")
                            ? document.getElementById("rowWithHeaderAndForm")
                                .clientHeight
                            : 305),
                        350
                      ) + //If it has not been rendered yet, guess it will be 305px high once it is rendered. On small screens, make it at least 350px big.
                      "px",
                  }}
                >
                  <VehicleList
                    vehicleList={observable(
                      this.state.originalArrayOfVehicles
                        .filter((vehicleModel) =>
                          vehicleModel
                            .toString()
                            .includes(this.state.stringForFiltering)
                        )
                        .sort((vehicleModel1, vehicleModel2) => {
                          if (this.state.sortingCriterion === "ID")
                            return vehicleModel1.id - vehicleModel2.id;
                          else if (this.state.sortingCriterion === "name") {
                            if (vehicleModel1.name < vehicleModel2.name)
                              return -1;
                            else if (vehicleModel1.name > vehicleModel2.name)
                              return 1;
                            else return 0;
                          } else if (
                            this.state.sortingCriterion === "abbreviation"
                          ) {
                            if (
                              vehicleModel1.abbreviation <
                              vehicleModel2.abbreviation
                            )
                              return -1;
                            else if (
                              vehicleModel1.abbreviation >
                              vehicleModel2.abbreviation
                            )
                              return 1;
                            else return 0;
                          }
                          throw new Error(
                            "Sorting criterion appears not to be set!"
                          );
                        })
                    )}
                    setCurrentName={this.setCurrentName}
                    setCurrentAbbreviation={this.setCurrentAbbreviation}
                    setCurrentImageURL={this.setCurrentImageURL}
                    setCurrentID={this.setCurrentID}
                  />
                </td>
              </tr>
            </table>
          </Route>
        </Switch>
      </Router>
    );
  }
}

export { App };
